const Flight = require('../models/flight');
const messages = require('../i18n/message_en.json');
const Result = require('./../common/response');
class FlightsController {
    getFlights() {
        return new Promise((resolve, reject) => {
            Flight.getAll().then(
                flights => {
                    let result = new Result(messages.stateResponse.SUCCESS_STATE, flights);
                    resolve(result);
                },
                error => {
                    let result = new Result(messages.stateResponse.FAIL_STATE);
                    reject(result);
                }
            );
        });
    }
    find(data){
        return new Promise((resolve, reject) => {
            Flight.findTrip(data).then(
                flights => {
                    let result = new Result(messages.stateResponse.SUCCESS_STATE, flights);
                    console.log(result);
                    resolve(result);
                },
                error => {
                    let result = new Result(messages.stateResponse.FAIL_STATE);
                    reject(result);
                }
            );
        });
    };

    buyTrip(data){
        return new Promise((resolve, reject) => {
            Flight.findById(id, (err, flight) => {
                if (!err && flight) {
                    flight.count--;
                    flight.save(
                        (err) => {
                            if (err) {
                                let result = new Result(messages.stateResponse.FAIL_STATE);
                                reject(result);
                            } else {
                                let result = new Result(messages.stateResponse.SUCCESS_STATE, flight);
                                resolve(result);
                            }
                        }
                    )
                } else {
                    let result = new Result(messages.stateResponse.FAIL_STATE);
                    reject(result);
                }
            });
        });
    }
}
module.exports = new FlightsController();
