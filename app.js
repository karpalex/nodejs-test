const express = require('express');
const bodyParser = require('body-parser');
const config = require('./config/config.json');
const app = express();
app.use(express.static('public'));
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));
app.get('/', function (req, res) {
    res.render(index);
});

app.use(require('./routes/index'));

app.listen(config.port, function () {
    console.log('app listening on port' + config.port);
});
