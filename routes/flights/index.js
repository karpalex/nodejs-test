const express = require('express');
const routes = require('./flights-routes');
const router = express.Router();

router.route('/')
    .get(routes.getFlights.bind(routes));
router.route('/')
    .post(routes.findTrip.bind(routes));
router.route('/:id')
    .post(routes.buyTrip.bind(routes));
module.exports = router;