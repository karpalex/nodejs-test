const Routes  = require('../index/routes');
const flightsController = require("../../controllers/flights.js");

class FlightRoutes extends  Routes {
    getFlights(req, res, next) {
        flightsController.getFlights()
            .then(this.response(res))
            .catch(this.response(res));
    };
    findTrip(req, res, next){
        flightsController.find(JSON.parse(req.body.data))
            .then(this.response(res))
            .catch(this.response(res));
    }
    buyTrip(req, res, next){
        const {id} = req.params;
        console.log(id);
        flightsController.buyTrip(id)
            .then(this.response(res))
            .catch(this.response(res));
    }
}

module.exports = new FlightRoutes();