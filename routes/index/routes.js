const HttpError = require('../../errors/http-error');
const {NOT_FOUND_ERROR_CODE} = require('../../errors/error-codes');
const messages = require('../../i18n/message_en.json');

class Routes {

    checkIfEntityFound(msg) {
        return entity => {
            if (!entity) {
                throw new HttpError(NOT_FOUND_ERROR_CODE, msg || messages.notFound)
            }
            return entity;
        }
    }

    response (res, statusCode) {
        statusCode = statusCode || 200;
        return entity => {
            entity = entity || {};
            res.status(statusCode).send({entity});
        }
    }
}

module.exports = Routes;