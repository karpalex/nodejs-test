const express = require('express');
const router = express.Router();

router.use('/*', (req, res, next) => {
    res.header('Access-Control-Allow-Origin', "*");
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    next();
});

router.use('/api/flights', require('../flights'));

module.exports = router;