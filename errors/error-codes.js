const ErrorCodes = {
    NOT_FOUND_ERROR_CODE: 404,
};

module.exports = Object.freeze(ErrorCodes);