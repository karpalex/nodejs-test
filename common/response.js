class Result {
    constructor(msg, body = {}) {
        this.msg = msg;
        this.body = body;
    }
}
module.exports = Result;