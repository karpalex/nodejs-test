const mongoose = require('mongoose');
const config = require('../config/config.json');

mongoose.connect(config.mongoose);
mongoose.Promise = global.Promise;

module.exports = mongoose;