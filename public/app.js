/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var _app = __webpack_require__(1);
	
	var _app2 = _interopRequireDefault(_app);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	_app2.default.start();

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _table = __webpack_require__(2);
	
	var _table2 = _interopRequireDefault(_table);
	
	var _dashboard = __webpack_require__(5);
	
	var _dashboard2 = _interopRequireDefault(_dashboard);
	
	var _buyForm = __webpack_require__(4);
	
	var _buyForm2 = _interopRequireDefault(_buyForm);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var App = function () {
	    function App() {
	        _classCallCheck(this, App);
	    }
	
	    _createClass(App, [{
	        key: 'start',
	        value: function start() {
	            this.createView({
	                rows: [_dashboard2.default.ui, {
	                    cols: [_buyForm2.default.ui, _table2.default.ui]
	                }]
	            });
	        }
	    }, {
	        key: 'createView',
	        value: function createView(view) {
	            this.currentView && this.currentView.destructor && this.currentView.destructor();
	            this.currentView = webix.ready(function () {
	                webix.ui(view);
	            });
	        }
	    }]);
	
	    return App;
	}();
	
	exports.default = new App(); // todo: add config into arguments

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	
	var _app = __webpack_require__(1);
	
	var _app2 = _interopRequireDefault(_app);
	
	var _buyWindow = __webpack_require__(3);
	
	var _buyWindow2 = _interopRequireDefault(_buyWindow);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var ui = {
	    rows: [{
	        id: "flights",
	        view: 'datatable',
	        resizeColumn: true,
	        select: "row",
	        columns: [{ id: "from", header: "From", adjust: true, fillspace: true }, { id: "to", header: "To", adjust: true, fillspace: true }, { id: "date", header: "Date", width: 150, sort: "date" }, { id: "price", header: "Price", css: "number", width: 95, sort: "int", format: webix.i18n.priceFormat }, { id: "count", header: "Tickets", css: "number", width: 65, sort: "int" }, {
	            id: "book",
	            header: "Booking",
	            width: 100
	        }]
	    }]
	};
	exports.default = {
	    ui: ui
	};

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	
	var _buyForm = __webpack_require__(4);
	
	var _buyForm2 = _interopRequireDefault(_buyForm);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var ui = {
	    view: "window",
	    id: "win2",
	    width: 300,
	    position: "center",
	    modal: true,
	    head: "Buy tickets",
	    body: webix.copy(_buyForm2.default.ui)
	};
	
	exports.default = {
	    ui: ui
	};

/***/ },
/* 4 */
/***/ function(module, exports) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	var ui = {
	
	    view: "form", width: 500, id: "form", elements: [{ view: "text", name: "from", label: "From", placeholder: "Select departure point" }, { view: "text", name: "to", label: "To", placeholder: "Select destination" }, { view: "datepicker", label: "Departure Date", value: new Date(), format: "%d  %M %Y" }, {
	        view: "datepicker",
	        id: "datepicker2",
	        name: "date",
	        label: "Return Date",
	        format: "%d  %M %Y",
	        hidden: true
	    }, { height: 10 }, {
	        view: "button", id: "submit", type: "form", value: "", inputWidth: 140, align: "center",
	        click: function click() {
	            var items = this.getFormView().getValues();
	            debugger;
	            webix.ajax().post("http://127.0.0.1:3000/api/flights", { data: items }, function (text) {
	                $$('flights').clearAll();
	                $$('flights').parse(JSON.parse(text).entity.body);
	            });
	        }
	    }, {}, {
	        elementsConfig: {
	            labelWidth: 100, labelAlign: "left"
	        }
	    }]
	
	};
	
	exports.default = {
	    ui: ui
	};

/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	
	var _app = __webpack_require__(1);
	
	var _app2 = _interopRequireDefault(_app);
	
	var _buyWindow = __webpack_require__(3);
	
	var _buyWindow2 = _interopRequireDefault(_buyWindow);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var ui = {
	    rows: [{
	        view: 'toolbar',
	        cols: [{}, {
	            view: 'button',
	            type: 'icon',
	            icon: 'cog',
	            width: 30, // todo: add click action
	            click: function click() {
	                var window = webix.ui(_buyWindow2.default.ui);
	                window.show();
	            }
	
	        }, {
	            view: 'button',
	            type: 'icon',
	            icon: 'sign-out',
	            width: 30
	        }]
	    }]
	};
	
	exports.default = {
	    ui: ui
	};

/***/ }
/******/ ]);
//# sourceMappingURL=app.js.map