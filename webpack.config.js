var path = require('path');
var webpack = require('webpack');

module.exports = {
    entry: './resources/assets/js/init',
    output: {
        path: path.join(__dirname, 'public'),
        filename: 'app.js'
    },
    module: {
        loaders: [
            {
                loader: 'babel-loader',
                test: path.join(__dirname, 'resources/assets/js'),
                query: {
                    presets: 'es2015'
                },
            }
        ]
    },
    plugins: [
        // Avoid publishing files when compilation fails
        new webpack.NoErrorsPlugin()
    ],
    stats: {
        // Nice colored output
        colors: true
    },
    // Create Sourcemaps for the bundle
    devtool: 'source-map',
};