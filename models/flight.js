const mongoose = require('../libs/mongoose')
    , Schema = mongoose.Schema;

const flightSchema = new Schema({
    from: String,
    to: String,
    date: {type: Date, default: Date.now},
    price: {type: Number, default: 0},
    count: {type: Number, default: 0}
});

flightSchema.statics.findTrip = (function (data) {
   return this.find({
        from: data.from,
        to:data.to
   });
});

flightSchema.statics.getAll = (function () {
    return this.find({})
});

module.exports = mongoose.model('Flight', flightSchema);