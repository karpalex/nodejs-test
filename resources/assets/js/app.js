import table from './views/table';
import toolbar from './views/dashboard';
import buyForm from './views/buyForm';
class App {
    start() {
        this.createView({
            rows: [
                toolbar.ui,
                {
                    cols: [
                        buyForm.ui,
                        table.ui
                    ]
                }
            ]
        });
    }

    createView(view) {
        this.currentView && this.currentView.destructor && this.currentView.destructor();
        this.currentView = webix.ready(function () {
            webix.ui(view);
        })
    }
}

export default new App(); // todo: add config into arguments
