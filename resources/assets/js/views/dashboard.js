import app from './../app';
import buyWindow from './buyWindow'
const ui = {
    rows: [
        {
            view: 'toolbar',
            cols: [
                {},
                {
                    view: 'button',
                    type: 'icon',
                    icon: 'cog',
                    width: 30, // todo: add click action
                    click: function () {
                        let window = webix.ui(buyWindow.ui);
                        window.show();
                    }

                },
                {
                    view: 'button',
                    type: 'icon',
                    icon: 'sign-out',
                    width: 30,
                }
            ]
        }

    ]
};

export default {
    ui: ui
};