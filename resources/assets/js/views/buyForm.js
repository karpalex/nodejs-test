const ui = {

        view: "form", width: 500, id: "form", elements: [
        {view: "text", name: "from", label: "From", placeholder: "Select departure point"},
        {view: "text", name: "to", label: "To", placeholder: "Select destination"},
        {view: "datepicker", label: "Departure Date", value: new Date(), format: "%d  %M %Y"},
        {
            view: "datepicker",
            id: "datepicker2",
            name: "date",
            label: "Return Date",
            format: "%d  %M %Y",
            hidden: true
        },

        {height: 10},
        {
            view: "button", id:"submit", type: "form", value: "", inputWidth: 140, align: "center",
            click: function () {
                var items = this.getFormView().getValues();
                debugger;
                webix.ajax().post("http://127.0.0.1:3000/api/flights", {data: items}, function (text) {
                    $$('flights').clearAll();
                    $$('flights').parse(JSON.parse(text).entity.body);
                });
            }
        },

        {},
        {
            elementsConfig: {
                labelWidth: 100, labelAlign: "left"
            }
        }
    ]

};

export default {
    ui: ui
}