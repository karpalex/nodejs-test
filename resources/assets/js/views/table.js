import app from '../app';
import buyWindow from './buyWindow';
const ui = {
    rows: [
        {
            id: "flights",
            view: 'datatable',
            resizeColumn: true,
            select: "row",
            columns: [
                {id: "from", header: "From", adjust: true, fillspace: true},
                {id: "to", header: "To", adjust: true, fillspace: true},
                {id: "date", header: "Date", width: 150, sort: "date"},
                {id: "price", header: "Price", css: "number", width: 95, sort: "int", format: webix.i18n.priceFormat},
                {id: "count", header: "Tickets", css: "number", width: 65, sort: "int"},
                {
                    id: "book",
                    header: "Booking",
                    width: 100,
                }
            ],
        },
    ]
};
export default {
    ui: ui,
};