import buyForm from './buyForm';
const ui = {
    view:"window",
    id:"win2",
    width:300,
    position:"center",
    modal:true,
    head:"Buy tickets",
    body:webix.copy(buyForm.ui)
};

export default {
    ui: ui
}